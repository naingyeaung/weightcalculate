﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MealPlannerAPI.Migrations
{
    public partial class MealGoal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MealGoal",
                table: "Dishes",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MealGoal",
                table: "Dishes");
        }
    }
}
