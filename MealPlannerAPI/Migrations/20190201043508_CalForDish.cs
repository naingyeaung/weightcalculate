﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MealPlannerAPI.Migrations
{
    public partial class CalForDish : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "TotalCaloreisPerUnit",
                table: "Dishes",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalCaloreisPerUnit",
                table: "Dishes");
        }
    }
}
