﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MealPlannerAPI.Migrations
{
    public partial class WrongSpelling : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TotalCaloreisPerUnit",
                table: "Dishes",
                newName: "TotalCaloriesPerUnit");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TotalCaloriesPerUnit",
                table: "Dishes",
                newName: "TotalCaloreisPerUnit");
        }
    }
}
