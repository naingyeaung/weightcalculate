﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MealPlannerAPI.Entities;
using MealPlannerAPI.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace MealPlannerAPI.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class CustomerController : Controller
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _context;

        public CustomerController(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            IConfiguration configuration,
            ApplicationDbContext context
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _context = context;
        }

        [HttpPost]
        public ActionResult Create(Customer model)
        {
            var userid = _userManager.GetUserId(User);
            var customer = _context.Customers.Where(x => x.UserID == userid).FirstOrDefault();
            if (customer == null)
            {
                return NotFound();
            }

            customer.Gender = model.Gender;
            customer.Height = model.Height;
            customer.Age = model.Age;
            customer.Weight = model.Weight;
            customer.Activity = model.Activity;
            customer.Goal = model.Goal;
            _context.SaveChanges();

            var data = new
            {
                code=200,
                message="success",
                customer
            };

            return Ok(data);
        }

        public ActionResult GetMealPlan()
        {
            var currentUser = _userManager.GetUserId(User);
            var customer = _context.Customers.Where(x => x.UserID == currentUser).FirstOrDefault();
            if (customer != null)
            {
                var data = Calculate(customer);
                return Ok(data);
            }
            else
            {
                return NotFound();
            }
        }

        public Object Calculate(Customer model)
        {
            var BMR = 0.0;

            model.Weight = model.Weight * 0.45359237;
            model.Height = model.Height * 2.54;
            if (model.Gender == Customer.GenderType.Male)
            {
                BMR = 66 + (13.7 * model.Weight) + (5*model.Height) - (6.8*model.Age);
            }
            else
            {
                BMR = 655 + (9.6 * model.Weight) + (1.8 * model.Height) - (4.7 * model.Age);
            }

            var totalCaloriesPerDay = 0.0;
            if (model.Activity == Customer.ActivityType.Sedentary)
            {
                totalCaloriesPerDay = BMR * 1.2;
            }
            else if (model.Activity == Customer.ActivityType.LightlyActive)
            {
                totalCaloriesPerDay = BMR * 1.375;
            }
            else if (model.Activity == Customer.ActivityType.ModeratelyActive)
            {
                totalCaloriesPerDay = BMR * 1.55;
            }
            else if (model.Activity == Customer.ActivityType.VeryActive)
            {
                totalCaloriesPerDay = BMR * 1.725;
            }
            else if (model.Activity == Customer.ActivityType.ExtraActive)
            {
                totalCaloriesPerDay = BMR * 1.9;
            }
            else
            {

            }

            

            if (model.Goal == Customer.GoalType.FatLoss)
            {
                BMR -= 1000;
            }
            else if (model.Goal == Customer.GoalType.Maintenance)
            {

            }
            else if (model.Goal == Customer.GoalType.WeightGain)
            {
                BMR += 500;
            }

            model.Goal = model.Goal == Customer.GoalType.Maintenance ? (model.Weight > 150 ? Customer.GoalType.FatLoss : Customer.GoalType.WeightGain) : model.Goal;

            var breakfastBMR = (BMR / 100) * 30;
            var lunchBMR = (BMR / 100) * 45;
            var dinnerBMR = (BMR / 100) * 25;

            var breakfasts = _context.Dishes.Where(x => x.TypeOfMeal == Dish.MealType.Breakfast && x.MealGoal == model.Goal);
            var lunchs = _context.Dishes.Where(x => x.TypeOfMeal == Dish.MealType.Lunch && x.MealGoal == model.Goal);
            var dinners = _context.Dishes.Where(x => x.TypeOfMeal == Dish.MealType.Dinner && x.MealGoal == model.Goal);

            List<Dish> allBreakFasts = new List<Dish>();
            List<Dish> allLunchs = new List<Dish>();
            List<Dish> allDinners = new List<Dish>();
            for(int i=0; i < 4; i++)
            {
                Random breakfastRnd = new Random();
                var firstBreakfast = breakfasts.OrderBy(x => breakfastRnd.Next());
                allBreakFasts.AddRange(firstBreakfast);

                Random lunchRnd = new Random();
                var firstLunch = lunchs.OrderBy(x => lunchRnd.Next());
                allLunchs.AddRange(firstLunch);

                Random dinnerRnd = new Random();
                var firstDinner = dinners.OrderBy(x => dinnerRnd.Next());
                allDinners.AddRange(firstDinner);

            }

            var breakfastArray = allBreakFasts.ToArray();
            var lunchArray = allLunchs.ToArray();
            var dinnerArray = allDinners.ToArray();

            List<Object> allMeals = new List<object>();

            for(int i=0; i<breakfastArray.Length; i++)
            {
                var day = i + 1;
                var breakfast = breakfastArray[i];
                var breakfastGram = breakfastBMR / breakfast.TotalCaloriesPerUnit;

                var breakfastData = new
                {
                    breakfast.ID,
                    breakfast.Name,
                    Photo = "http://naingyeaung1998-001-site1.dtempurl.com/img/"+breakfast.Photo+".jpg",
                    breakfast.MealGoal,
                    breakfastGram = Math.Round(breakfastGram)

                };

                var lunch = lunchArray[i];
                var lunchGram = lunchBMR / lunch.TotalCaloriesPerUnit;

                var lunchData = new
                {
                    lunch.ID,
                    lunch.Name,
                    Photo = "http://naingyeaung1998-001-site1.dtempurl.com/img/" + lunch.Photo + ".jpg",
                    lunch.MealGoal,
                    lunchGram = Math.Round(lunchGram)
                };


                var dinner = dinnerArray[i];
                var dinnerGram = dinnerBMR / dinner.TotalCaloriesPerUnit;

                var dinnerData = new
                {
                    dinner.ID,
                    dinner.Name,
                    Photo = "http://naingyeaung1998-001-site1.dtempurl.com/img/" + dinner.Photo + ".jpg",
                    dinner.MealGoal,
                    dinnerGram = Math.Round(dinnerGram)
                };

                var data = new
                {
                    breakfastData,
                    lunchData,
                    dinnerData,
                    day = day.ToString()
                };

                allMeals.Add(data);

                
            }

            var returnData = new
            {
                code = 200,
                message = "success",
                dishes = allMeals
            };

            return returnData;
        }
    }
}