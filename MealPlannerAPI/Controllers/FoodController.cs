﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MealPlannerAPI.Entities;
using MealPlannerAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace MealPlannerAPI.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class FoodController : ControllerBase
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _context;

        public FoodController(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            IConfiguration configuration,
            ApplicationDbContext context
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _context = context;
        }

        [HttpPost]
        public ActionResult Create(Food model)
        {
            model.ID = Guid.NewGuid();
            _context.Foods.Add(model);
            _context.SaveChanges();

            return Ok(model);
        }
    }
}