﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MealPlannerAPI.Entities;
using MealPlannerAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace MealPlannerAPI.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _context;

        public OrderController(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            IConfiguration configuration,
            ApplicationDbContext context
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _context = context;
        }

        public ActionResult Index()
        {
            List<Object> ords = new List<object>();
            var orders = _context.Orders.OrderBy(x => x.OrderDate).Include(x => x.OrderDishs);
            foreach (var order in orders)
            {
                List<Object> dishs = new List<object>();

                foreach (var dish in order.OrderDishs)
                {
                    var dis = _context.Dishes.Where(x => x.ID == dish.DishID).FirstOrDefault();
                    if (dis == null)
                    {
                        return NotFound();
                    }
                    var data = new
                    {
                        dis.ID,
                        dis.Name,
                        Photo = "http://naingyeaung1998-001-site1.dtempurl.com/img/" + dis.Photo + ".jpg",
                        dish.DishQtyInGram,
                        DeliveryDate = dish.DeliveryDate.Date.ToString("MMMM-dd-yyyy")
                    };

                    dishs.Add(data);
                }

                var orderdata = new
                {
                    order.ID,
                    OrderDate = order.OrderDate.Date.ToString("MMMM-dd-yyyy"),
                    order.Address,
                    order.Phone,
                    dishes = dishs
                };

                ords.Add(orderdata);
            }

            var returndata = new
            {
                code = 200,
                message = "success",
                orders = ords
            };

            return Ok(returndata);
        }

        public ActionResult Create(Order model)
        {
            var currentUser = _userManager.GetUserId(User);
            var customerId = _context.Customers.Where(x => x.UserID == currentUser).FirstOrDefault().ID;
            model.ID = Guid.NewGuid();
            model.OrderDate = DateTime.UtcNow.AddHours(6).AddMinutes(30);
            model.CustomerID = customerId;

            _context.Orders.Add(model);
            _context.SaveChanges();

            return Ok(model);

        }

        public ActionResult GetCustomerOrders()
        {
            var currentUser = _userManager.GetUserId(User);
            var customerId = _context.Customers.Where(x => x.UserID == currentUser).FirstOrDefault().ID;

            List<Object> ords = new List<object>();
            

            var orders = _context.Orders.Where(x => x.CustomerID == customerId).Include(x=>x.OrderDishs).OrderBy(x => x.OrderDate);

            foreach(var order in orders)
            {
                List<Object> dishs = new List<object>();

                foreach (var dish in order.OrderDishs)
                {
                    var dis = _context.Dishes.Where(x => x.ID == dish.DishID).FirstOrDefault();
                    if (dis == null)
                    {
                        return NotFound();
                    }
                    var data = new
                    {
                        dis.ID,
                        dis.Name,
                        Photo = "http://naingyeaung1998-001-site1.dtempurl.com/img/" + dis.Photo + ".jpg",
                        dish.DishQtyInGram,
                        DeliveryDate = dish.DeliveryDate.Date.ToString("MMMM-dd-yyyy")
                    };

                    dishs.Add(data);
                }

                var orderdata = new
                {
                    order.ID,
                    OrderDate = order.OrderDate.Date.ToString("MMMM-dd-yyyy"),
                    order.Address,
                    order.Phone,
                    dishes = dishs
                };

                ords.Add(orderdata);
            }

            var returndata = new
            {
                code = 200,
                message = "success",
                orders = ords
            };

            return Ok(returndata);
        }
    }
}