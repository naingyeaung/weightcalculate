﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MealPlannerAPI.Entities;
using MealPlannerAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace MealPlannerAPI.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class DishController : ControllerBase
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _context;

        public DishController(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            IConfiguration configuration,
            ApplicationDbContext context
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _context = context;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var dishes = _context.Dishes.Include(x=>x.DishFoods);

            List<Object> allDishes = new List<object>();
            foreach(var dish in dishes)
            {
                List<Object> foods = new List<object>();
                foreach(var dishFood in dish.DishFoods)
                {
                    var food = _context.Foods.Where(x => x.ID == dishFood.FoodID).Select(x=>new {
                        x.ID,
                        x.Name,
                        x.CaloriesPerUnit
                    }).FirstOrDefault();
                    foods.Add(food);
                }
                var data = new
                {
                    dish.ID,
                    dish.Name,
                    dish.ExtraCaloriesForDish,
                    dish.Photo,
                    foods
                };
                allDishes.Add(data);
            }
            var d = new
            {
                dishes = allDishes
            };
            return Ok(d);   
        }

        [HttpPost]
        public ActionResult Create(Dish model)
        {
            model.ID = Guid.NewGuid();
            _context.Dishes.Add(model);
            _context.SaveChanges();
            return Ok(model);
        }
    }
}