﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MealPlannerAPI.Models
{
    public class Order
    {
        public Guid ID { get; set; }

        public Guid CustomerID { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public DateTime OrderDate { get; set; }

        public double TotalAmount { get; set; }

        public Customer Customer { get; set; }

        public ICollection<OrderDish> OrderDishs { get; set; }

        public Order()
        {

        }
    }
}
