﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MealPlannerAPI.Models
{
    public class Customer
    {
        public Guid ID { get; set; }

        public string UserID { get; set; }

        public string Name { get; set; }

        public GenderType Gender { get; set; }

        public double Height { get; set; }

        public int Age { get; set; }

        public double Weight { get; set; }

        public ActivityType Activity { get; set; }

        public GoalType Goal { get; set; }

        public int DietDays { get; set; }

        public DateTime CreatedOn { get; set; }

        //foreignkey
        public IdentityUser NetUser { get; set; }

        public Customer()
        {

        }

        public enum GenderType
        {
            Male,
            Female
        }

        public enum ActivityType
        {
            Sedentary,
            LightlyActive,
            ModeratelyActive,
            VeryActive,
            ExtraActive
        }

        public enum GoalType
        {
            FatLoss,
            Maintenance,
            WeightGain
        }
    }
}
