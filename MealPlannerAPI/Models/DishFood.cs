﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MealPlannerAPI.Models
{
    public class DishFood
    {
        public Guid ID { get; set; }

        public Guid DishID { get; set; }

        public Guid FoodID { get; set; }

        public Dish Dish { get; set; }

        public Food Food { get; set; }

        public DishFood()
        {

        }
    }
}
