﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MealPlannerAPI.Models
{
    public class OrderDish
    {
        public Guid ID { get; set; }

        public Guid OrderID { get; set; }

        public Guid DishID { get; set; }

        public double DishQtyInGram { get; set; }

        public DateTime DeliveryDate { get; set; }

        public Order Order { get; set; }

        public Dish Dish { get; set; }

        public OrderDish()
        {

        }
    }
}
