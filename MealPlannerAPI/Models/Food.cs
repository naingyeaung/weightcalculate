﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MealPlannerAPI.Models
{
    public class Food
    {
        public Guid ID { get; set; }

        public string Name { get; set; }

        public double CaloriesPerUnit { get; set; }

        public FoodType TypeOfFood { get; set; }

        public ICollection<DishFood> DishFoods { get; set; }

        public enum FoodType
        {
            Meat,
            Vegetable,
            Fruit,
            Snack,
            Drink
        }

        
    }
}
