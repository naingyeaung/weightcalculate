﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MealPlannerAPI.Models
{
    public class Dish
    {
        public Guid ID { get; set; }

        public string Name { get; set; }

        public double ExtraCaloriesForDish { get; set; }

        public double TotalCaloriesPerUnit { get; set; }

        public MealType TypeOfMeal { get; set; }

        public Customer.GoalType MealGoal { get; set; }

        public string Photo { get; set; }

        public ICollection<DishFood> DishFoods { get; set; }

        public enum MealType
        {
            Breakfast,
            Lunch,
            Dinner
        }

    }
}
